
from django.views.generic import ListView
from .models import Post

#here, inherrtiging the ListView allows us to specify 3 variables that make listing items in the database easily
#those variables are model, the template where you want the list, and the context object, whcih you can name anything and appears to be a dict.
#the context object is the 


class HomePageView(ListView):
    model = Post
    template_name = 'home.html'
    context_object_name = 'objects_list' # new
    print(context_object_name)